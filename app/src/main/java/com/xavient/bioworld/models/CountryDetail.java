package com.xavient.bioworld.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public class CountryDetail {
    @SerializedName("currencies")
    @Expose
    private List<Currency> currencies = null;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("numericCode")
    @Expose
    private String numericCode;

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }

}
