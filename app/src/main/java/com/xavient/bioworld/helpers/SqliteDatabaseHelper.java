package com.xavient.bioworld.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.xavient.bioworld.models.Country;

import java.util.ArrayList;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public class SqliteDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = SqliteDatabaseHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "bioworld.db";
    private static final int DATABASE_VERSION = 1;
    private static SqliteDatabaseHelper instance;

    private static final String TABLE_COUNTRY = "country";
    //Columns
    private static final String NUMERIC_CODE = "numeric_code";
    private static final String NAME = "name";

    public static synchronized SqliteDatabaseHelper getInstance(Context ctx) {
        if (instance == null)
            instance = new SqliteDatabaseHelper(ctx);
        return instance;
    }

    private SqliteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data.");

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_COUNTRY + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NUMERIC_CODE + " TEXT,"
                + NAME + " TEXT)");
    }

    /**
     * Handling duplicate case.
     * when duplicate it will update previous one
     * otherwise insert new row
     */
    public void addCountry(Country country) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(NUMERIC_CODE, country.getNumericCode());
            values.put(NAME, country.getName());
            int row = db.update(TABLE_COUNTRY, values, NAME + "=?", new String[]{country.getName()});
            if (row < 1) {
                db.insert(TABLE_COUNTRY, null, values);
            }
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }
    }

    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> list = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = getReadableDatabase();
            String query = "SELECT * from " + TABLE_COUNTRY;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Country country = new Country();
                    String code = cursor.getString(1);
                    String name = cursor.getString(2);
                    country.setName(name);
                    country.setNumericCode(code);
                    list.add(country);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return list;
    }

}
