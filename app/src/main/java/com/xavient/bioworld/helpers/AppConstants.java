package com.xavient.bioworld.helpers;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public interface AppConstants {
    String COUNTRY="country";
    String SELECTED_INDEX="selected_index";
    int VIEW_COUNTRY_REQUEST_CODE=1001;
}
