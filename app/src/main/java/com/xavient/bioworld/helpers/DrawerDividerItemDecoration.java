package com.xavient.bioworld.helpers;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;


public class DrawerDividerItemDecoration extends RecyclerView.ItemDecoration {
    private static int PADDING = 0;
    private Drawable mDivider;

    public DrawerDividerItemDecoration(Resources resources, int drawableId) {
        mDivider = resources.getDrawable(drawableId);
    }

    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int right = parent.getWidth() - parent.getPaddingRight();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(PADDING, top, right, bottom);
            mDivider.draw(c);
        }
    }
}