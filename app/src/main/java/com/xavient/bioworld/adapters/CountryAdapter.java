package com.xavient.bioworld.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xavient.bioworld.R;
import com.xavient.bioworld.models.Country;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder>{
    private Context context;
    private ArrayList<Country> countries;
    private CountyClickListener listener;

    public CountryAdapter(Context context, ArrayList<Country> countries,CountyClickListener listener) {
        this.context = context;
        this.countries = countries;
        this.listener = listener;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.item_country, parent, false);
       return  new CountryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, final int position) {
        final Country country=countries.get(position);
        holder.txtCountryName.setText(country.getName());
        holder.imgChecked.setVisibility(country.isViewed()?View.VISIBLE:View.INVISIBLE);
        holder.bind(country,position);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    /*Interface for RecycleView*/
    public interface CountyClickListener {
         void onCountryClick(Country country,int pos);
    }
    class CountryViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.rlItem)
        RelativeLayout rlItem;
        @BindView(R.id.imgChecked) ImageView imgChecked;
        @BindView(R.id.txtCountryName) TextView txtCountryName;


        public CountryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /*bind view for click */
        public void bind(final Country country, final int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   listener.onCountryClick(country,position);
                }
            });
        }
    }
}
