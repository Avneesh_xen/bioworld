package com.xavient.bioworld.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xavient.bioworld.R;
import com.xavient.bioworld.adapters.CountryAdapter;
import com.xavient.bioworld.apis.RestApiClient;
import com.xavient.bioworld.apis.RestApiService;
import com.xavient.bioworld.helpers.AlertMessageHelper;
import com.xavient.bioworld.helpers.AppConstants;
import com.xavient.bioworld.helpers.ConnectivityHelper;
import com.xavient.bioworld.helpers.DrawerDividerItemDecoration;
import com.xavient.bioworld.helpers.SqliteDatabaseHelper;
import com.xavient.bioworld.models.Country;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountriesActivity extends AppCompatActivity implements CountryAdapter.CountyClickListener, AppConstants, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycleView)
    RecyclerView mRecycleView;
    private ArrayList<Country> countriesList;
    private CountryAdapter mCountryAdapter;
    @BindView((R.id.progress))
    ProgressBar mProgress;
    @BindView(R.id.txtInternet)
    TextView txtInternet;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwiperefresh;
    private boolean isRefreshing;
    private ArrayList<Country> allViewedCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        setupRecyclerAdapter();
        mSwiperefresh.setOnRefreshListener(this);
        if (ConnectivityHelper.isConnected(CountriesActivity.this))
            makeCountryRequest();
        else
            txtInternet.setVisibility(View.VISIBLE);

    }

    /*initialize mRecycleView set decoration and adapter*/
    private void setupRecyclerAdapter() {
        countriesList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CountriesActivity.this);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRecycleView.addItemDecoration(new DrawerDividerItemDecoration(getResources(), R.drawable.line_divider));
        mCountryAdapter = new CountryAdapter(this, countriesList, this);
        mRecycleView.setAdapter(mCountryAdapter);
    }

    /*this is network request for get all country we are using retrofit for network request */
    private void makeCountryRequest() {
        if (!mSwiperefresh.isRefreshing())
            mProgress.setVisibility(View.VISIBLE);
        RestApiService apiService = RestApiClient.getApiService();
        final Call<ArrayList<Country>> countryRequest = apiService.getCountries();
        countryRequest.enqueue(new Callback<ArrayList<Country>>() {
            @Override
            public void onResponse(Call<ArrayList<Country>> call, Response<ArrayList<Country>> response) {
                if (response != null && response.isSuccessful()) {
                    ArrayList<Country> countryArrayList = response.body();
                    updateCountries(countryArrayList);
                } else {
                    mProgress.setVisibility(View.GONE);
                    mSwiperefresh.setRefreshing(false);
                    isRefreshing = false;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Country>> call, Throwable t) {
                mProgress.setVisibility(View.GONE);
                isRefreshing = false;
                mSwiperefresh.setRefreshing(false);
                AlertMessageHelper.showMessage(CountriesActivity.this, getString(R.string.unable_to_fetch_data_please_check_your_internet_connection));
            }
        });
    }

    /*Update country after network response and fill it into mRecycleView*/
    private void updateCountries(ArrayList<Country> countryArrayList) {
        countriesList.clear();
        mProgress.setVisibility(View.GONE);
        mSwiperefresh.setRefreshing(false);
        isRefreshing = false;
        SqliteDatabaseHelper instance = SqliteDatabaseHelper.getInstance(this);
        allViewedCountry = instance.getAllCountry();
        for (int i = 0; i < countryArrayList.size(); i++) {
            boolean isExists = isCountryExists(countryArrayList.get(i));
            countryArrayList.get(i).setViewed(isExists);
        }
        countriesList.addAll(countryArrayList);
        mCountryAdapter.notifyDataSetChanged();
    }

    private boolean isCountryExists(Country country) {
        for (int i = 0; i < allViewedCountry.size(); i++) {
            Country country1 = allViewedCountry.get(i);
            if (country1.getNumericCode().equals(country.getNumericCode())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == VIEW_COUNTRY_REQUEST_CODE) {
            int selectedIndex = data.getIntExtra(SELECTED_INDEX, 0);
            countriesList.get(selectedIndex).setViewed(true);
            mCountryAdapter.notifyDataSetChanged();
        }
    }

    /*RecycleView Item click then open next activity for get details*/
    @Override
    public void onCountryClick(Country country, int pos) {
        Intent intentDetail = new Intent(this, CountryDetailActivity.class);
        intentDetail.putExtra(COUNTRY, country);
        intentDetail.putExtra(SELECTED_INDEX, pos);
        startActivityForResult(intentDetail, VIEW_COUNTRY_REQUEST_CODE);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onRefresh() {
        if (isRefreshing) {
            return;
        }
        isRefreshing = true;
        mSwiperefresh.setRefreshing(true);
        txtInternet.setVisibility(View.GONE);
        makeCountryRequest();
    }

}
