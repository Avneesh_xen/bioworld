package com.xavient.bioworld.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.guardanis.imageloader.ImageRequest;
import com.xavient.bioworld.R;
import com.xavient.bioworld.apis.RestApiClient;
import com.xavient.bioworld.apis.RestApiService;
import com.xavient.bioworld.helpers.AppConstants;
import com.xavient.bioworld.helpers.ConnectivityHelper;
import com.xavient.bioworld.helpers.SqliteDatabaseHelper;
import com.xavient.bioworld.models.Country;
import com.xavient.bioworld.models.CountryDetail;
import com.xavient.bioworld.models.Currency;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryDetailActivity extends AppCompatActivity implements AppConstants, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgFlag)
    AppCompatImageView imgFlag;
    @BindView(R.id.txtCurrencyName)
    TextView txtCurrencyName;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.txtInternet)
    TextView txtInternet;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwiperefresh;
    Country country;
    int selectedIndex;
    private boolean isRefreshing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);
        ButterKnife.bind(this);
        country = getIntent().getParcelableExtra(COUNTRY);
        selectedIndex = getIntent().getIntExtra(SELECTED_INDEX, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(country.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initFlagSize();
        mSwiperefresh.setOnRefreshListener(this);
        if (ConnectivityHelper.isConnected(CountryDetailActivity.this)) {
            callGetCountryDetail();
        } else {
            txtInternet.setVisibility(View.VISIBLE);
        }
    }

    /*set flag display size we set only width here. height will be set in aspect ratio*/
    private void initFlagSize() {
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        int min = width < height ? width : height;
        imgFlag.getLayoutParams().width = (int) (min - getResources().getDimension(R.dimen.dp_5));
    }

    /* Make network request for all selected detail*/
    private void callGetCountryDetail() {
        if (!mSwiperefresh.isRefreshing())
            progress.setVisibility(View.VISIBLE);
        RestApiService apiService = RestApiClient.getApiService();
        final Call<ArrayList<CountryDetail>> countryRequest = apiService.getCountryDetails(country.getName());
        countryRequest.enqueue(new Callback<ArrayList<CountryDetail>>() {
            @Override
            public void onResponse(Call<ArrayList<CountryDetail>> call, Response<ArrayList<CountryDetail>> response) {
                mSwiperefresh.setRefreshing(false);
                isRefreshing = false;
                if (response != null && response.isSuccessful()) {
                    ArrayList<CountryDetail> countryArrayList = response.body();
                    showCountryDetail(countryArrayList);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CountryDetail>> call, Throwable t) {
                mSwiperefresh.setRefreshing(false);
                progress.setVisibility(View.GONE);
                isRefreshing = false;
            }
        });
    }

    /*Show all county after network success responses*/
    private void showCountryDetail(ArrayList<CountryDetail> countryArrayList) {
        SqliteDatabaseHelper.getInstance(this).addCountry(country);
        if (countryArrayList.size() > 0) {
            final CountryDetail countryDetail = countryArrayList.get(0);
            ImageRequest.create(imgFlag)
                    .setSuccessCallback(new ImageRequest.ImageSuccessCallback() {
                        @Override
                        public void onImageReady(ImageRequest request, Drawable result) {
                            progress.setVisibility(View.GONE);
                            showAllCurrency(countryDetail);
                        }
                    })
                    .setShowStubOnExecute(true)
                    .setShowStubOnError(true)
                    .setTargetUrl(countryDetail.getFlag())
                    .execute();
        }
    }


    /*Set Currency text*/
    private void showAllCurrency(CountryDetail countryDetail) {
        StringBuilder sbCurrency = new StringBuilder();
        sbCurrency.append(getString(R.string.currency) + ": ");
        for (Currency currency : countryDetail.getCurrencies()) {
            sbCurrency.append("( ");
            sbCurrency.append(currency.getSymbol());
            sbCurrency.append(" ) ");
            sbCurrency.append(currency.getName());
            sbCurrency.append("\n");
        }
        txtCurrencyName.setText(sbCurrency.toString());
    }

    /**
     * Actionbar or Toolbar back button handling
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(SELECTED_INDEX, selectedIndex);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRefresh() {
        if (isRefreshing) {
            return;
        }
        isRefreshing = true;
        mSwiperefresh.setRefreshing(true);
        callGetCountryDetail();
        txtInternet.setVisibility(View.GONE);
    }
}
