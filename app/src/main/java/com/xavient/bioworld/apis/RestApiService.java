package com.xavient.bioworld.apis;

import com.xavient.bioworld.models.Country;
import com.xavient.bioworld.models.CountryDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public interface RestApiService {
    @GET("v2/all?fields=name;numericCode")
    Call<ArrayList<Country>> getCountries();

    @GET("v2/name/{country_name}?fields=name;numericCode;currencies;flag")
    Call<ArrayList<CountryDetail>> getCountryDetails(@Path("country_name") String countryName);
}
