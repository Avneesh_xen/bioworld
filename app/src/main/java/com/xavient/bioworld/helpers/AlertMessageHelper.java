package com.xavient.bioworld.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;

import com.xavient.bioworld.R;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public class AlertMessageHelper {
    public static void showMessage(Context context, String message){
        AlertDialog.Builder builder= new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
