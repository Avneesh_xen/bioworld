package com.xavient.bioworld.apis;


import com.xavient.bioworld.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ryadav3 on 8/4/2017.
 */

public class RestApiClient {
    private static Retrofit retrofit;
    private static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static RestApiService getApiService(){
        return getClient().create(RestApiService.class);
    }
}
